const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors    = require('cors');
const {getUser, insertUser, updateData } = require("./mongo");
const {ObjectId}    = require('mongodb');
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const staticPath = path.resolve(__dirname, '../static');
app.use(express.static(staticPath));


app.put('/insert_user', (req, res)=> {
    console.log('req.body: ', req.body);
    insertUser([req.body], 'AppUsers', (err, data) => {
        console.log(err, data);
        res.send(data);
    });
});


app.get('/get_user', (req, res)=>{
    getUser({}, 'AppUsers', data=>{
        res.send(data);
    });
});
app.post('/update_user', (req, res)=>{
    console.log('req.body: ', req.body);
    const {_id, FirstName, LastName} = req.body.emp;
    updateUser({_id: ObjectId(_id)}, {FirstName, LastName}, 'AppUsers', (err, data)=>{
        console.log(err, data);
        res.send(data);
    });
});

app.post('/delete_user', (req, res)=> {
    console.log('req.body: ', req.body);
    deleteData({_id: ObjectId(req.body._id)}, 'AppUsers', data => {
        res.send(data);
    })
});

app.post('/logging_user', (req, res) => {
    console.log('req.body: ', req.body);
    getUser(req.body, 'AppUsers', (data) => {
       console.log(data);
       const token = {token: Math.random()};
       updateData(req.body, token,
           'AppUsers', ()=>{
           data[0].token = token.token;
               res.send(data);
               console.log('mongo return', data);
           })
    });
});


/*node port*/
const port = 5047;
app.listen(port, function () {
    console.log("server stared at : ", port);
});
