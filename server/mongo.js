const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'Chat';

let dbObj;
MongoClient.connect(url, function(err, client) {
    console.log("Connected successfully to server");
    dbObj = client.db(dbName);
});

module.exports = {
    getUser: function (query, collection, cb) {
        console.log('query: ', query);
        console.log('collection: ', collection);
        dbObj.collection(collection).find(query).toArray(function(err, result) {
            cb(result);
        });
    },
    insertUser: function (data, collection, cb) {
        dbObj.collection(collection).insertMany(data, cb);
    },
    updateData: function (query, newVal, collection, cb) {
        dbObj.collection(collection).updateOne(query, {$set: newVal}, { upsert: true }, function(err, result) {
            cb(err, result);
            console.log('newVal: ', query, newVal)
        });
    }
};
