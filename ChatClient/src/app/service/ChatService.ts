import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// constants data
import { END_POINTS } from './endpoints';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};


@Injectable()
export class ChatService {
  constructor(private http: HttpClient) {
  }
  insertUser(data: { FirstName: any; LastName: any; country: any; gender: any; mobile: any; email: any; password: any; repassword: any; }, cb: { (resp: any): void; (arg0: Object): void; }) {
    this.http.put(END_POINTS.BASE + END_POINTS.INSERT_USER,
      // tslint:disable-next-line:no-shadowed-variable
      data, httpOptions).subscribe(data => {
      console.log('oyo: ', data);
      cb(data);
    });
  }
  getUser(cb: { (data: string): void; (data: any): void; (arg0: Object): void; }) {
    this.http.get(END_POINTS.BASE + END_POINTS.GET_USER).subscribe(data => {
      console.log('oyo: ', data);
      cb(data);
    });
  }
  fetchUserCred(data: { email: string; password: string; }, cb: { (resp: any): void; (arg0: Object): void; }) {
    this.http.post(END_POINTS.BASE + END_POINTS.LOGGING_USER,
      data, httpOptions).subscribe(rsp => {
      console.log('oyo: ', rsp);
      cb(rsp);
    });
  }
}
