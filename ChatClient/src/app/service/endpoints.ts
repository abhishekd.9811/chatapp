export const END_POINTS = {
  BASE: 'http://localhost:5047/',
  GET_USER: 'get_user',
  INSERT_USER: 'insert_user',
  DELETE_USER: 'delete_user',
  UPDATE_USER: 'update_user',
  LOGGING_USER: 'logging_user'
};
