import {Component} from '@angular/core';
import { ChatService} from '../service/ChatService';

@Component({
  selector: 'app-after-login',
  templateUrl: './app.after.login.html',
  styleUrls: ['./app.after.login.css']
})
export class AppAfterLoginComponent {
  signedUser;
  cred = {email: '', password: ''};
  constructor(private chatService: ChatService) {
    chatService.getUser(data => {
      console.log('signed user data' + data);
      this.signedUser = data;
    });
  }

  login() {
    this.chatService.fetchUserCred(this.cred, (resp) => {
      console.log(resp);
      localStorage.setItem('userdata', JSON.stringify(resp));
          });
  }
}
