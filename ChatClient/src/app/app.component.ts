import { Component } from '@angular/core';
import { ChatService } from './service/ChatService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ChatClient';
  nav = ['HOME', 'NEWS', 'CONTACT', 'ABOUT'];
  activeNav;
  UserData;
  cred = {email: '', password: ''};
  activeView = 'login';
  constructor(public chatService: ChatService) {
    chatService.getUser(data => {
      console.log(data);
      this.UserData = data;
    });
    this.activeNav = this.nav[0];
  }

  login() {
    this.chatService.fetchUserCred(this.cred, (resp) => {
      console.log(resp);
      localStorage.setItem('userdata', JSON.stringify(resp));
    });
  }

  openReg() {
    this.activeView = 'reg';
  }

  closeReg() {
    this.activeView = 'login';
  }

  insertUser(FirstName, LastName, country, gender, mobile, email, password, repassword) {
    const DataToSend = {FirstName, LastName, country, gender, mobile, email, password, repassword};
    this.chatService.insertUser(DataToSend, (resp) => {
      console.log('Response from server: ', resp);
    });
    this.activeView = 'login';
  }

}
