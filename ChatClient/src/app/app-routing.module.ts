import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppAfterLoginComponent} from './after-login/app.after.login';
import { AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';


const appRoutes: Routes = [
  {path: 'signed',
    component: AppAfterLoginComponent
  }];
@NgModule({
  imports: [RouterModule.forRoot(
    appRoutes, {enableTracing: true}), FormsModule, CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
